from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city} {state}",
              "per_page": 1}
    
    response = requests.get(url,params,headers=headers)
    data = json.loads(response.content)
    return {"picture_url": data["photos"][0]["src"]["original"] }

def get_weather(city, state):
    print(f"{city} {state}")

    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit={1}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geo_url)
    print(f"*********** {response}")
    data = response.json()
    print(f"*********** {data}")
    try:
        lat = data[0]['lat']
        lon = data[0]['lon']
    except IndexError:
        return {"message": "bad boy"}
    # print(f"************ {lat} {lon}")

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    weather_data = requests.get(weather_url)
    weather_data = json.loads(weather_data.content)
    

    weather = {"temp": f"{weather_data['main']['temp']} fahrenheit",
               "description": weather_data['weather'][0]['description']}
    return weather