import json
from django.http import JsonResponse
from common.json import ModelEncoder, IDExtractor
from .models import Attendee, ConferenceVO 
from django.views.decorators.http import require_http_methods


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]

class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):

    if request.method == "GET":
        try:
            print(conference_vo_id)
            attendees = Attendee.objects.filter(conference=conference_vo_id)
        except Attendee.DoesNotExist:
            return JsonResponse({"message": "Invalid Attendee ID"})

        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
            safe=False
        )
    else: # POST
        content = json.loads(request.body)
        try:
            conference_href = f'/api/conferences/{conference_vo_id}/'
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name","created", "conference"]
    encoders = {"conference": ConferenceVODetailEncoder()}


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):

    if request.method == "GET":
        try:
            attendees = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return JsonResponse({"message": "Invalid Attendee ID"})
        
        return JsonResponse(
            attendees,
            encoder=AttendeeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"message": count > 0})
    else: # PUT 
        content = json.loads(request.body)
        try: 
            conference_info = content.get("conference")
            conference_id =  conference_info.get("cid") #int(conference_info.get("href").split('/')[-2])
            if isinstance(conference_id, int):
                conference = ConferenceVO.objects.get(id=conference_id)
            elif isinstance(conference_id, str):
                conference = ConferenceVO.objects.get(name=conference_id)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
    Attendee.objects.filter(id=id).update(**content)
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )
